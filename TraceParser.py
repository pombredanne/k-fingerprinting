import sys
if sys.version_info[0] < 3:
    raise Exception("Must be using Python 3")

import hashlib, struct

class Packet(object):
    DIR_INCOMING = -1
    DIR_OUTGOING = 1

    def __init__(self, timestamp, direction):
        assert (type(timestamp) is int) and (timestamp >= 0)
        assert (type(direction) is int) and (direction in (Packet.DIR_INCOMING, Packet.DIR_OUTGOING))
        self.__timestamp = timestamp
        self.__direction = direction

    def __str__(self):
        return 'ts= {ts} dir= {dir}'.format(ts=self.__timestamp, dir=self.__direction)

    def get_timestamp(self):
        return self.__timestamp

    def get_direction(self):
        return self.__direction



def _get_trace_id(pkt_list):
    # version should be incremented whenever we change the way we
    # compute the trace id
    version = 1

    # pack the version
    buf = bytearray()
    buf += struct.pack('<I', version)

    # for each packet, pack its unsigned timestamp and signed
    # direction
    for pkt in pkt_list:
        buf += struct.pack('<Ii', pkt.get_timestamp(), pkt.get_direction())
        pass

    assert len(buf) == (4 * (1 + (2 * len(pkt_list))))

    # use the md5 digest (which is of immutable type "bytes") of the
    # buf as the trace id
    trace_id = hashlib.md5(buf).digest()

    return trace_id


class _MalformedTraceLine(Exception):
    pass


def _sha1sum_file(fname):
    hash_sha1 = hashlib.sha1()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha1.update(chunk)
            pass
        return hash_sha1.digest()
    pass

def _parse_trace_line(trace_line):
    parts = trace_line.strip().split()

    try:
        pkt_timestamp = int(parts[0])
        pkt_size = int(parts[1])
        pkt_direction = Packet.DIR_OUTGOING if pkt_size > 0 else Packet.DIR_INCOMING

        return pkt_timestamp, pkt_direction
        pass
    except IndexError:
        raise _MalformedTraceLine()
    pass

def _trace_line_to_pkt(first_pkt_timestamp, trace_line):
    pkt_timestamp, pkt_direction = _parse_trace_line(trace_line)
    # We convert timestamps to relative timestamps
    return Packet(pkt_timestamp - first_pkt_timestamp, pkt_direction)

def parse_file(trace_file):
    '''

    Given a file containing a network trace, where each line represents a packet and has the format:

    <timestamp> <packet_size>

    will return a tuple (trace_id, list of Packet objects).

    Negative packet_size represents incoming packet while positive
    represents outgoing one. the returned list of packets will have
    timestamps shifted to be relative to the first packet, i.e., the
    first packet returned will have timestamp = 0.

    "trace_id" will be md5 hexdigest of the relevant contents of the
    file. it could have been just the md5sum of the original
    tracefile, but the tracefile might contain things we do not care
    about, so instead we md5sum the representation of the trace file
    that matters, i.e., the returned list packets (plus a little
    version info). (kinda like "content-based hashing"?)

    '''

    pkt_list = []
    with open(trace_file) as trace_file_handle:
        first_trace_line = trace_file_handle.readline()
        first_pkt_timestamp, _ = _parse_trace_line(first_trace_line)

        pkt_list.append(_trace_line_to_pkt(first_pkt_timestamp, first_trace_line))

        # we will tolerate a malformed line if it's the last line in the file
        seen_malformed_line = False
        for trace_line in trace_file_handle:
            assert not seen_malformed_line
            try:
                pkt_list.append(_trace_line_to_pkt(first_pkt_timestamp, trace_line))
                pass
            except _MalformedTraceLine:
                seen_malformed_line = True
                pass
            pass
        pass

    trace_id = _get_trace_id(pkt_list)

    return trace_id, pkt_list

def parse_dill_trace_file(fpath):
    '''return trace_id, pkt_list tuple where trace_id is the sha1sum of
    the byte-content of the file, and pkt list is list of Packet
    objects

    '''

    import dill
    with open(fpath, 'rb') as fp:
        dill_pkts = dill.load(fp)

        # should be list of (timestamp, packet size) tuples. the first
        # timestamp must be 0
        assert type(dill_pkts) is list
        assert dill_pkts[0][0] == 0

        pkt_list = list(map(
            lambda dill_pkt: Packet(dill_pkt[0],
                                    Packet.DIR_OUTGOING if dill_pkt[1] > 0 else Packet.DIR_INCOMING),
            dill_pkts))
        pass

    trace_id = _sha1sum_file(fpath)
    return trace_id, pkt_list

