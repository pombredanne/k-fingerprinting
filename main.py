import os
import argparse
from kfingerprint import KFingerprint
import dill, logging
import json

alexa_num_websites = -1 # -1 means use all available sites
alexa_num_training_instances = 60
alexa_num_test_instances = 40
hs_num_websites = 30
hs_num_training_instances = 60
hs_num_test_instances = 40
unmonitored_num_training_websites = 1000
unmonitored_num_test_websites = 4000

def __parse_arguments():
    parser = argparse.ArgumentParser(description = 'k-fingerprint Implementation')
    parser.add_argument('-s', '--scenario', dest = 'scenario', action = 'store', choices = ['openworld', 'closedworld'],
                        default = 'openworld', help = 'Testing scenario: Open World/ Closed World')
    parser.add_argument('-d', '--dataset', dest = 'dataset', action = 'store', choices = ['alexa', 'hs', 'both'],
                        required = True, help = 'Dataset to use')
    parser.add_argument('-knn', type = int, dest = 'knn', action = 'store', default = 6,
                        help = 'Neighbours to use for classification')
    parser.add_argument('-n', type = int, dest = 'numruns', action = 'store', default = 50,
                        help = 'number of classification runs')

    parser.add_argument(
        "--log-level", dest='log_level', default='INFO',
        help="set log level")

    default_trace_features_db_fname = 'trace-features-db.dill'
    parser.add_argument(
        "--db",
        metavar="db", default=default_trace_features_db_fname,
        help='path to trace features db.'
        ' (default: {default})'.format(default=default_trace_features_db_fname))

    parser.add_argument(
        '--alternate-test-data-spec',
        dest='alternate_test_data_spec',
        help='path to json file containing mapping from website label to the '
        'directory containing the raw traces (i.e., the "N.txt" files) to be '
        'used for testing for that label')

    parser.add_argument(
        '--only-labels-with-alternate-test-data',
        dest='only_labels_with_alternate_test_data',
        action='store_true', default=False,
        help='train and test only labels that have alternate test data (i.e., those specified in --alternate-test-data-spec)')

    # this is directory that contains the "Alexa_Monitored,"
    # "HS_Monitored," and "Unmonitored" dirs
    parser.add_argument(
        "data_dir", help="path to root of data (traces) directory")

    return parser.parse_args()

def __run_tests(args):

    with open(args.db, 'rb') as fp:
        trace_features_db = dill.load(fp)
        pass

    traces_dir = args.data_dir
    alexa_traces_dir = os.path.join(traces_dir, 'Alexa_Monitored')
    hs_traces_dir = os.path.join(traces_dir, 'HS_Monitored')
    unmonitored_traces_dir = os.path.join(traces_dir, 'Unmonitored')

    alternate_test_data_dirs = {}
    if args.alternate_test_data_spec:
        with open(args.alternate_test_data_spec) as fp:
            alternate_test_data_dirs = json.load(fp)
            pass
        pass

    if args.only_labels_with_alternate_test_data:
        # must have some stuff in alternate_test_data_dirs in order to
        # use only_labels_with_alternate_test_data
        assert len(alternate_test_data_dirs) > 0
        pass

    k_fingerprint_impl = KFingerprint(alexa_traces_dir=alexa_traces_dir,
                                      hs_traces_dir=hs_traces_dir,
                                      unmonitored_traces_dir=unmonitored_traces_dir,
                                      num_unmonitored_websites=unmonitored_num_training_websites + unmonitored_num_test_websites,
                                      trace_features_db=trace_features_db,
                                      alternate_test_data_dirs=alternate_test_data_dirs,
                                      only_labels_with_alternate_test_data=args.only_labels_with_alternate_test_data)

    if args.scenario == 'closedworld':
        if args.dataset == 'alexa':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        alexa_num_websites = alexa_num_websites,
                                        alexa_num_training_instances = alexa_num_training_instances if not args.only_labels_with_alternate_test_data else -1,
                                        alexa_num_test_instances = alexa_num_test_instances if not args.only_labels_with_alternate_test_data else -1)
        if args.dataset == 'hs':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        hs_num_websites = hs_num_websites,
                                        hs_num_training_instances = hs_num_training_instances,
                                        hs_num_test_instances = hs_num_test_instances)
        if args.dataset == 'both':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        alexa_num_websites = alexa_num_websites,
                                        alexa_num_training_instances = alexa_num_training_instances,
                                        alexa_num_test_instances = alexa_num_test_instances,
                                        hs_num_websites = hs_num_websites,
                                        hs_num_training_instances = hs_num_training_instances,
                                        hs_num_test_instances = hs_num_test_instances)
    elif args.scenario == 'openworld':
        if args.dataset == 'alexa':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        alexa_num_websites = alexa_num_websites,
                                        alexa_num_training_instances = alexa_num_training_instances,
                                        alexa_num_test_instances = alexa_num_test_instances,
                                        unmonitored_num_training_websites = unmonitored_num_training_websites,
                                        unmonitored_num_test_websites = unmonitored_num_test_websites)
        if args.dataset == 'hs':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        hs_num_websites = hs_num_websites,
                                        hs_num_training_instances = hs_num_training_instances,
                                        hs_num_test_instances = hs_num_test_instances,
                                        unmonitored_num_training_websites = unmonitored_num_training_websites,
                                        unmonitored_num_test_websites = unmonitored_num_test_websites)
        if args.dataset == 'both':
            k_fingerprint_impl.run_test(knn = args.knn,
                                        numruns = args.numruns,
                                        alexa_num_websites = alexa_num_websites,
                                        alexa_num_training_instances = alexa_num_training_instances,
                                        alexa_num_test_instances = alexa_num_test_instances,
                                        hs_num_websites = hs_num_websites,
                                        hs_num_training_instances = hs_num_training_instances,
                                        hs_num_test_instances = hs_num_test_instances,
                                        unmonitored_num_training_websites = unmonitored_num_training_websites,
                                        unmonitored_num_test_websites = unmonitored_num_test_websites)

def main():
    args = __parse_arguments()

    # assuming loglevel is bound to the string value obtained from the
    # command line argument. Convert to upper case to allow the user to
    # specify --log=DEBUG or --log=debug
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log_level)
    rootLogger = logging.getLogger()
    assert not rootLogger.hasHandlers()
    # using "my_handler" member is hacky but needed because there is
    # no "remove all handlers" api, so we have to remember our handler
    # in order to remove it later
    rootLogger.my_handler = logging.StreamHandler()
    rootLogger.my_formatter = logging.Formatter(
        fmt="%(levelname) -10s %(asctime)s %(module)s:%(lineno)d, %(funcName)s(): %(message)s")
    rootLogger.my_handler.setFormatter(rootLogger.my_formatter)
    rootLogger.addHandler(rootLogger.my_handler)
    assert rootLogger.hasHandlers()
    rootLogger.setLevel(numeric_level)

    __run_tests(args)

if __name__ == '__main__':
    main()
