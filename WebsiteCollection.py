import os
import logging
import TraceParser

g_features_version = 1

class Website(object):
    UNMONITORED_LABEL = "unmonitored"

    def __init__(self, label):
        self.instances = []
        self.__label = label

        # use this variable for alternate test instances
        self.alt_test_instances = []
        pass

    def append_instance(self, instance):
        self.instances.append(instance)

    def append_alt_test_instance(self, instance):
        if self.alt_test_instances is None:
            self.alt_test_instances = []
            pass
        self.alt_test_instances.append(instance)
        pass

    def get_instances(self):
        return list(self.instances)

    def get_alt_test_instances(self):
        return list(self.alt_test_instances)

    def get_label(self):
        return self.__label


def _get_features_of_traces_in_dir(directory, trace_features_db):
    def _should_proess_trace_file(directory, trace_file):
        # skip the "0" traces, which are potentially "cold" traces
        # (e.g., dns hasn't been cached, OS buffer cache is cold,
        # etc. and if chrome is reused to load the same page multiple
        # times, etc.)
        if os.path.basename(directory)[0] == '.' or trace_file[0] == '.' \
           or (trace_file.endswith('/0.txt') or trace_file == '0.txt') \
           or (trace_file.endswith('/0.dill') or trace_file == '0.dill'):
            return False
        return True

    instances = []

    for fname in sorted(os.listdir(directory)):
        logging.debug('file name {}'.format(fname))
        trace_fpath = os.path.join(directory, fname)

        _, file_extension = os.path.splitext(fname)
        if file_extension != '.dill':
            raise Exception('please make sure only dill trace files are present '
                            '(trace file "{fpath}" encountered)'.format(fpath=trace_fpath))

        if not _should_proess_trace_file(directory, fname):
            continue

        trace_id, _ = TraceParser.parse_dill_trace_file(trace_fpath)
        try:
            features_container_obj = trace_features_db[trace_id][g_features_version]
            pass
        except KeyError:
            raise Exception('unable to look up features version {features_version} '
                            'for trace file "{fpath}"'.format(
                                features_version=g_features_version,
                                fpath=trace_fpath))
        assert features_container_obj.version == g_features_version

        instances.append(features_container_obj.features)
        pass

    return instances

class WebsiteCollection(object):
    def __init__(self, *, alexa_website_traces_dir, hs_traces_dir, unmonitored_traces_dir,
                 num_unmonitored_websites, trace_features_db,
                 alternate_test_data_dirs={},
                 only_labels_with_alternate_test_data=False):
        self.__trace_features_db = trace_features_db
        self.__alexa_websites = self.__populate_websites(
            traces_dir = alexa_website_traces_dir,
            alternate_test_data_dirs = alternate_test_data_dirs,
            only_labels_with_alternate_test_data = only_labels_with_alternate_test_data)
        self.__hidden_services = self.__populate_websites(
            traces_dir = hs_traces_dir,
            alternate_test_data_dirs = alternate_test_data_dirs,
            only_labels_with_alternate_test_data = only_labels_with_alternate_test_data)
        # alternate test data is not applicable to unmonitored sites
        self.__unmonitored_websites = self.__populate_websites(traces_dir = unmonitored_traces_dir,
                                                               label = Website.UNMONITORED_LABEL,
                                                               num_unmonitored_websites = num_unmonitored_websites)

    def __populate_websites(self, *, traces_dir, label = None,
                            num_unmonitored_websites = None,
                            alternate_test_data_dirs = {},
                            only_labels_with_alternate_test_data = False):
        logging.debug('read in traces from traces dir: {}'.format(traces_dir))

        if alternate_test_data_dirs:
            # do not specify alternate test data for unmonitored sites
            assert label is None
            pass

        websites = {}
        num_websites = 0
        for directory, _, fnames in os.walk(traces_dir):
            if not fnames:
                continue

            website_label = label if label else directory

            if only_labels_with_alternate_test_data and \
               (website_label not in alternate_test_data_dirs):
                # skip labels for which we don't have alternate test
                # data
                continue

            logging.debug('(sub)dir: "{directory}", label to use: "{label}"'.format(
                directory=directory, label=website_label))

            website = websites.get(website_label, Website(website_label))
            websites[website_label] = website

            website.instances.extend(
                _get_features_of_traces_in_dir(directory, self.__trace_features_db))
 
            # done reading in trace files from one dir... see if we
            # should use alternate test data

            if label is None:
                assert website_label is directory

                alternate_test_data_dir = alternate_test_data_dirs.get(website_label, None)
                if alternate_test_data_dir is not None:
                    print('NOTE!! using traces in "{dpath}" for test data for label "{label}"'.format(
                        dpath=alternate_test_data_dir, label=website_label))
                    website.alt_test_instances.extend(
                        _get_features_of_traces_in_dir(
                            alternate_test_data_dir, self.__trace_features_db))
                    pass
                pass

            num_websites += 1
            if num_unmonitored_websites and num_websites >= num_unmonitored_websites:
                return websites.values()

            pass

        return list(websites.values())

    def get_alexa_websites(self):
        return self.__alexa_websites

    def get_hidden_services(self):
        return self.__hidden_services

    def get_unmonitored_websites(self):
        return self.__unmonitored_websites

