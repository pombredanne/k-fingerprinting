import math
import numpy as np
from TraceParser import Packet

import FeaturesContainers

class FeatureExtractor(object):
    def __init__(self, feature_vec_len = 175):
        #Each feature vector is of same length (ensure either by truncating or padding 0s)
        self.__feature_vec_len = feature_vec_len

    def __separate_in_out_pkts(self, pkt_list):
        in_pkts = [pkt for pkt in pkt_list if pkt.get_direction() == Packet.DIR_INCOMING]
        out_pkts = [pkt for pkt in pkt_list if pkt.get_direction() == Packet.DIR_OUTGOING]
        return in_pkts, out_pkts

    def __force_length(self, lst, min_len = None, max_len = None):
        new_list = lst
        if min_len:
            new_list = lst if len(lst) >= min_len else lst.extend([0] * (min_len - len(lst)))
        if max_len:
            new_list = lst if len(lst) <= max_len else lst[:max_len]
        return new_list

    def __chunk(self, lst, chunk_len):
        for i in range(0, len(lst), chunk_len):
            yield lst[ i: i + chunk_len]

    def __alt_chunk(self, lst, chunk_len):
        step = len(lst) / float(chunk_len)
        last = 0.0
        while last < len(lst):
            yield lst[int(last):int(last + step)]
            last += step

    def __inter_pkt_times(self, pkt_timestamps):
        inter_pkt_times = [next_pkt_timestamp - pkt_timestamp for pkt_timestamp, next_pkt_timestamp in zip(pkt_timestamps, pkt_timestamps[1:] + [pkt_timestamps[0]])]
        return self.__force_length(inter_pkt_times, min_len = 1)

    def __inter_pkt_time_stats(self, pkt_timestamps, in_pkt_timestamps, out_pkt_timestamps):
        inter_pkt_time_stats = []

        inter_pkt_times = self.__inter_pkt_times(pkt_timestamps)
        in_inter_pkt_times = self.__inter_pkt_times(in_pkt_timestamps)
        out_inter_pkt_times = self.__inter_pkt_times(out_pkt_timestamps)

        inter_pkt_time_stats.extend([max(in_inter_pkt_times), max(out_inter_pkt_times), max(inter_pkt_times)])
        inter_pkt_time_stats.extend([np.mean(in_inter_pkt_times), np.mean(out_inter_pkt_times),np.mean(inter_pkt_times)])
        inter_pkt_time_stats.extend([np.std(in_inter_pkt_times), np.std(out_inter_pkt_times), np.std(inter_pkt_times)])
        inter_pkt_time_stats.extend([np.percentile(in_inter_pkt_times, 75), np.percentile(out_inter_pkt_times, 75), np.percentile(inter_pkt_times, 75)])

        return inter_pkt_time_stats

    def __time_percentile_stats(self, pkt_timestamps, in_pkt_timestamps, out_pkt_timestamps):
        time_percentile_stats = []

        for timestamp_list in in_pkt_timestamps, out_pkt_timestamps, pkt_timestamps:
            for nth_percentile in [25, 50, 75, 100]:
                time_percentile_stats.append(np.percentile(self.__force_length(timestamp_list, min_len = 1), nth_percentile))

        return time_percentile_stats

    def __number_pkt_stats(self, pkt_list, in_pkts, out_pkts):
        return [len(in_pkts), len(out_pkts), len(pkt_list)]

    def __first_and_last_30_pkts_stats(self, pkt_list):
        first_30_pkts = pkt_list[:30]
        last_30_pkts = pkt_list[-30:]

        in_first_30_pkts, out_first_30_pkts = self.__separate_in_out_pkts(first_30_pkts)
        in_last_30_pkts, out_last_30_pkts = self.__separate_in_out_pkts(last_30_pkts)

        return [len(in_first_30_pkts), len(out_first_30_pkts), len(in_last_30_pkts), len(out_last_30_pkts)]

    #concentration of outgoing packets in chunks of 20 packets
    def __pkt_concentration_stats(self, pkt_list):
        concentrations = []
        for chunk in self.__chunk(pkt_list, 20):
            _, out_chunk = self.__separate_in_out_pkts(chunk)
            concentrations.append(len(out_chunk))
        concentrations = self.__force_length(concentrations, min_len = 1)

        return np.std(concentrations), np.mean(concentrations), np.percentile(concentrations, 50), max(concentrations), concentrations

    #Average number packets sent and received per second
    def __number_pkts_per_sec_stats(self, pkt_list):
        last_pkt_second = int(math.ceil(pkt_list[-1].get_timestamp()))
        pkts_per_second = [0] * last_pkt_second

        for pkt in pkt_list:
            pkt_second = int(math.floor(pkt.get_timestamp()))
            pkts_per_second[min(pkt_second, last_pkt_second - 1)] += 1
        pkts_per_second = self.__force_length(pkts_per_second, min_len = 1)

        return np.mean(pkts_per_second), np.std(pkts_per_second), np.percentile(pkts_per_second, 50), min(pkts_per_second), max(pkts_per_second), pkts_per_second

    #Variant of packet ordering features from http://cacr.uwaterloo.ca/techreports/2014/cacr2014-05.pdf
    def __pkts_ordering_stats(self, pkt_list):
        in_pkt_ordering = []
        out_pkt_ordering = []

        for pkt_index in range(len(pkt_list)):
            if pkt_list[pkt_index].get_direction() == Packet.DIR_INCOMING:
                in_pkt_ordering.append(pkt_index)
            else:
                out_pkt_ordering.append(pkt_index)

        in_pkt_ordering = self.__force_length(in_pkt_ordering, min_len = 1)
        out_pkt_ordering = self.__force_length(out_pkt_ordering, min_len = 1)

        return np.mean(in_pkt_ordering), np.mean(out_pkt_ordering), np.std(in_pkt_ordering), np.std(out_pkt_ordering)

    def __percentage_in_out_stats(self, pkt_list, in_pkts, out_pkts):
        percentage_in = len(in_pkts) / float(len(pkt_list))
        percentage_out = len(out_pkts) / float(len(pkt_list))
        return percentage_in, percentage_out

    def extract(self, pkt_list):
        in_pkts, out_pkts = self.__separate_in_out_pkts(pkt_list)

        pkt_timestamps = [pkt.get_timestamp() for pkt in pkt_list]
        in_pkt_timestamps = [in_pkt.get_timestamp() for in_pkt in in_pkts]
        out_pkt_timestamps = [out_pkt.get_timestamp() for out_pkt in out_pkts]

        inter_pkt_time_stats = self.__inter_pkt_time_stats(pkt_timestamps, in_pkt_timestamps, out_pkt_timestamps)
        time_percentile_stats = self.__time_percentile_stats(pkt_timestamps, in_pkt_timestamps, out_pkt_timestamps)
        number_pkt_stats = self.__number_pkt_stats(pkt_list, in_pkts, out_pkts)
        first_and_last_30_pkts_stats = self.__first_and_last_30_pkts_stats(pkt_list)
        std_conc, avg_conc, med_conc, max_conc, conc = self.__pkt_concentration_stats(pkt_list)
        avg_per_sec, std_per_sec, med_per_sec, min_per_sec, max_per_sec, per_sec = self.__number_pkts_per_sec_stats(pkt_list)
        avg_order_in, avg_order_out, std_order_in, std_order_out = self.__pkts_ordering_stats(pkt_list)
        perc_in, perc_out = self.__percentage_in_out_stats(pkt_list, in_pkts, out_pkts)

        altconc = [sum(x) for x in self.__alt_chunk(conc, 70)]
        alt_per_sec = [sum(x) for x in self.__alt_chunk(per_sec, 20)]
        if len(altconc) == 70:
            altconc.append(0)
        if len(alt_per_sec) == 20:
            alt_per_sec.append(0)

        feature_vector = []
        feature_vector.extend(inter_pkt_time_stats)
        feature_vector.extend(time_percentile_stats)
        feature_vector.extend(number_pkt_stats)
        feature_vector.extend(first_and_last_30_pkts_stats)
        feature_vector.append(std_conc)
        feature_vector.append(avg_conc)
        feature_vector.append(avg_per_sec)
        feature_vector.append(std_per_sec)
        feature_vector.append(avg_order_out)
        feature_vector.append(avg_order_in)
        feature_vector.append(std_order_out)
        feature_vector.append(std_order_in)
        feature_vector.append(med_conc)
        feature_vector.append(med_per_sec)
        feature_vector.append(min_per_sec)
        feature_vector.append(max_per_sec)
        feature_vector.append(max_conc)
        feature_vector.append(perc_in)
        feature_vector.append(perc_out)
        feature_vector.extend(altconc)
        feature_vector.extend(alt_per_sec)
        feature_vector.append(sum(altconc))
        feature_vector.append(sum(alt_per_sec))
        feature_vector.append(sum(inter_pkt_time_stats))
        feature_vector.append(sum(time_percentile_stats))
        feature_vector.append(sum(number_pkt_stats))
        feature_vector.extend(conc)
        feature_vector.extend(per_sec)

        feature_vector = self.__force_length(feature_vector, min_len = self.__feature_vec_len, max_len = self.__feature_vec_len)

        return FeaturesContainers.FeaturesContainerV1(feature_vector)
