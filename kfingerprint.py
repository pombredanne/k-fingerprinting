import os
import dill
from WebsiteCollection import WebsiteCollection
from Classifier import ClassifierParams
from Classifier import Classifier
import logging


class KFingerprint(object):
    def __init__(self, *, alexa_traces_dir, hs_traces_dir, unmonitored_traces_dir,
                 num_unmonitored_websites, trace_features_db,
                 alternate_test_data_dirs={},
                 only_labels_with_alternate_test_data=False):
        self.__alexa_traces_dir = alexa_traces_dir
        self.__hs_traces_dir = hs_traces_dir
        self.__unmonitored_traces_dir = unmonitored_traces_dir

        self.__website_collection = WebsiteCollection(alexa_website_traces_dir=self.__alexa_traces_dir,
                                               hs_traces_dir=self.__hs_traces_dir,
                                               unmonitored_traces_dir=self.__unmonitored_traces_dir,
                                               num_unmonitored_websites=num_unmonitored_websites,
                                               trace_features_db=trace_features_db,
                                               alternate_test_data_dirs=alternate_test_data_dirs,
                                               only_labels_with_alternate_test_data=only_labels_with_alternate_test_data)

    def run_test(self, *, knn, alexa_num_websites = None, alexa_num_training_instances = None, alexa_num_test_instances = None,
                 hs_num_websites = None, hs_num_training_instances = None, hs_num_test_instances = None,
                 unmonitored_num_training_websites = None, unmonitored_num_test_websites = None,
                 numruns = 50):
        alexa_params = None
        hs_params = None
        unmonitored_params = None

        if alexa_num_websites and alexa_num_training_instances and alexa_num_test_instances:
            alexa_params = ClassifierParams(alexa_num_websites, alexa_num_training_instances, alexa_num_test_instances)
            logging.debug('alexa params: {}'.format(alexa_params))
            pass

        if hs_num_websites and hs_num_training_instances and hs_num_test_instances:
            hs_params = ClassifierParams(hs_num_websites, hs_num_training_instances, hs_num_test_instances)

        if unmonitored_num_training_websites and unmonitored_num_test_websites:
            unmonitored_params = ClassifierParams(1, unmonitored_num_training_websites, unmonitored_num_test_websites)
            logging.debug('unmonitored params: {}'.format(unmonitored_params))
            pass

        for i in range(numruns):
            classifier = Classifier(self.__website_collection)
            classifier.classifiy(knn, alexa_params, hs_params, unmonitored_params)
            pass

