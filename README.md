# k-FP

as of May 20, 2017, these are instructions to do k-fingerprinting:

* however you set up the trace collection, use `convert-traces.py` (in
  the `crawler` repo, in `giangs_branch`) to transform traces to
  python dill files. the outcome is that each trace is a dill file,
  containing a list of of packets, and each packet is a 2/3-element
  list/tuple <timestamp, packet size, peer addr>

* use `extract_features.py` in this repo to extract the features of
  the traces into a "db", e.g.:

```
find <traces-dir-for-one-experiment> -name "*.dill" | \
    xargs python ~/k-fingerprinting/extract_features.py extract-features \
    --num-workers <num-worker-processes> \
    --db <features-db.dill>
```

- `traces-dir-for-one-experiment`: contains dill trace files. doesn't
  matter the directory structure since we're doing `find -name
  "*.dill"`, but probably contains a directory for each page, and the
  traces for that page are in the page's dir. this structure is how
  the crawling script `crawl.py` sets up the directory.

- `features-db.dill`: where to save the extracted features. is both
  input and output: the script will read the db before processing the
  dill files, and updates the db as it goes.

each trace dill file is assigned an ID (which is the sha1 hash of the
file), and extracted features are stored into the features db: simple
a dict mapping from trace ID to feature vector.

if a trace already exists in the db, it will be skipped (NOTE: there
is versioning stuff, too, in case we make changes to features), so
this can be safely run multiple times.

* use `experiments/copy-tcpdumper-traces.py` in the `crawler` repo to
  copy the traces into a new directory, say, `traces-dir-for-kfp`, with
  structure that k-fingerprint wants. most relevantly, `traces-dir-for-kfp`
  will have `Alexa_Monitored` subdir, and at this point that's pretty
  much the only thing we've been using

* finally k-fingerprinting can be run like:

```
python main.py -s closedworld -d alexa <traces-dir-for-kfp> --db <features-db.dill>
```

---

*OLD instructions:*

Benchmarks for the [k-FP WF attack](http://www.homepages.ucl.ac.uk/~ucabaye/papers/k-fingerprinting.pdf)

The attack works on trace files containing direction and timing of packets.

To run first make sure all necessary libraries are installed (via ```requirements.txt```)

##### Steps to run
* Unzip the network traces: ```tar xvf data.tar.gz```
* Closed World:
    * ```python main.py -s closedworld -d alexa```
    * ```python main.py -s closedworld -d hs```
    * ```python main.py -s closedworld -d both```
* Open World:
    * ```python main.py -s openworld -d alexa```
    * ```python main.py -s openworld -d hs```
    * ```python main.py -s openworld -d both```
* Providing ```-f``` option forces feature regeneration. 
* ```--knn``` option contorls the number of neighbours used in classification in k-fingerprinting.
* Number of unmonitored websites used in training and testing can be changed in main.py. Make sure to re-generate features using ```-f``` option after changing the number of unmonitored websites used.

##### Disclaimer
```RF_fextract.py``` and ```k-FP.py``` is the old implementation and is not used while using above commands. To use it [look here](https://github.com/jhayes14/k-FP)
