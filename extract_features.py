
# extract and save features into pickle files

import sys, os, dill, argparse
import logging
import time
import multiprocessing, functools
import queue
import pdb, traceback, tempfile

import TraceParser
from FeatureExtracter import FeatureExtractor


g_default_worker_flush_interval = 50

# we're currently using features version 1. if you change the set of
# features or how features are computed etc, should update accordingly
g_features_version = 1


# will not raise any exception. return True if everything is good,
# otherwise return False
def _dump_to_disk(obj, fpath):
    try:
        logging.debug('dumping to fpath "{fpath}"'.format(fpath=fpath))
        tmpname = None
        # use current dir for temp file so that we can use os.rename,
        # otherwise /tmp would be used which might be on different
        # partition which will not work with os.rename()
        with tempfile.NamedTemporaryFile(dir='.', delete=False) as fp:
            tmpname = fp.name
            dill.dump(obj, fp, protocol=4)
            pass
        # logging.debug('tmpname "{tmpname}"'.format(tmpname=tmpname))
        os.rename(tmpname, fpath)
        return True

    except Exception as exc:
        exc_info = traceback.format_exc()
        logging.error('error trying to serialize an obj to file "{fpath}":'.format(fpath=fpath),
                      exc_info=exc_info)
        return False
    pass

def worker_func(*, task_queue, current_db, output_db_fpath,
                features_version=g_features_version,
                flush_interval=g_default_worker_flush_interval):
    assert output_db_fpath is not None

    my_new_db = {}
    try:
        # if it exists, read in its current data
        with open(output_db_fpath, 'rb') as fp:
            my_new_db = dill.load(fp)
            pass
        pass
    except FileNotFoundError:
        pass

    assert type(my_new_db) is dict

    num_results_not_flushed = 0
    queue_empty_exception = False


    # as soon as we encounter a disk write fail, we effectively stop
    # doing anything at all, but we will continue to consume all
    # available tasks so that the main process can quickly gracefully
    # exit, so we don't waste time keeping trying in face of
    # persistent/real errors/bugs
    disk_write_failed = False

    while True:
        try:
            # assumes that main process fills up the task queue first
            # before starting us workers and then doesn't add anymore
            # task. so, here, if queue is empty, then there will be no
            # more tasks
            trace_file = task_queue.get()
            if trace_file is None:
                break
            pass
        except queue.Empty:
            queue_empty_exception = True
            break

        if disk_write_failed:
            task_queue.task_done()
            continue

        try:
            trace_id, pkt_list = TraceParser.parse_dill_trace_file(trace_file)

            trace_info = current_db.get(trace_id, None) or my_new_db.get(trace_id, None)
            if (trace_info is None) or (features_version not in trace_info):
                trace_info = trace_info or {}
                features_container_obj = FeatureExtractor().extract(pkt_list)
                logging.debug('done extracting features from trace file "{}"'.format(trace_file))

                trace_info[features_version] = features_container_obj
                my_new_db[trace_id] = trace_info
                num_results_not_flushed += 1
                pass
            pass
        except Exception as exc:
            exc_info = traceback.format_exc()
            logging.error('error handling trace file "{trace_file}":'.format(trace_file=trace_file),
                          exc_info=exc_info)
            pass

        task_queue.task_done()

        if num_results_not_flushed and (0 == (num_results_not_flushed % flush_interval)):
            if not _dump_to_disk(my_new_db, output_db_fpath):
                disk_write_failed = True
                pass
            num_results_not_flushed = 0
            pass

        pass # end while loop

    if (not disk_write_failed) and (num_results_not_flushed > 0):
        logging.debug('done with tasks... write final results to disk')
        _dump_to_disk(my_new_db, output_db_fpath)
        pass

    if not queue_empty_exception:
        task_queue.task_done()
        pass
    pass

def _extract_features(args):
    num_workers = args.num_workers
    if num_workers is None:
        num_workers = max(1, int(os.cpu_count() / 2))
        pass

    #
    # each worker will get a reference to the current db, so that it
    # can skip any traces that are already in the db.
    #
    # for traces that the work has to process, it will save them into
    # its own per-worker db, using the file path given by the main
    # process
    #
    # after all workers are done, the main process will collect all
    # the workers' work into the db (and clean up the workers' dbs)
    #


    # this current_db will be passed to all child workers, but since
    # no one will be modifying it, linux copy-on-write should mean
    # only one copy exists
    current_db = {}
    try:
        with open(args.db, 'rb') as fp:
            current_db = dill.load(fp)
            pass
        pass
    except FileNotFoundError:
        pass

    assert type(current_db) is dict

    logging.info('launching {} workers'.format(num_workers))
    logging.info('number of traces given: {}'.format(len(args.trace_file)))

    multiprocessing.set_start_method('fork')

    task_queue = multiprocessing.JoinableQueue()

    # put all the tasks into the queue before starting workers
    for i, trace_file in enumerate(args.trace_file):
        task_queue.put(trace_file)
        pass

    worker_results_db_fpaths = []

    # use "None" task to let worker know there's no more work and it
    # can write its results to disk. we use this as a way to
    # synchronize: the main process waits for the the None tasks to be
    # done (i.e., workers have finished writing to disk) before
    # collecting their results
    for i in range(num_workers):
        task_queue.put(None)
        pass

    for i in range(num_workers):
        worker_results_db_fpath = 'worker_{}_results.dill'.format(i)
        worker_results_db_fpaths.append(worker_results_db_fpath)

        worker = multiprocessing.Process(
            target=worker_func,
            kwargs={'task_queue': task_queue,
                    'current_db': current_db,
                    'output_db_fpath': worker_results_db_fpath,
                    'flush_interval': args.worker_flush_interval,
                    },
            )
        worker.start()
        pass

    task_queue.join()

    logging.info('(maybe) collect any new results from workers into the db')

    need_write = False

    for new_results_db_fpath in worker_results_db_fpaths:
        try:
            with open(new_results_db_fpath, 'rb') as fp:
                logging.debug('collect from "{}"'.format(new_results_db_fpath))
                new_db = dill.load(fp)
                assert type(new_db) is dict
                current_db.update(new_db)
                need_write = True
                pass
            os.unlink(new_results_db_fpath)
            pass
        except FileNotFoundError:
            # workers might not have done any new work and thus not
            # saved any to disk
            pass
        except Exception as exc:
            exc_info = traceback.format_exc()
            logging.error('error collecting results from "{}":'.format(new_results_db_fpath),
                          exc_info=exc_info)
            pass
        pass

    if need_write:
        logging.info('write db ({size} entries) back to disk'.format(size=len(current_db)))
        _dump_to_disk(current_db, args.db)
        pass
    else:
        logging.info('no new work was found')
        pass

    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''

        extract features from trace files and save to/update trace database.
        the database is pickle/dilled file containing map from trace id to
        features. each worker will save its work to a 

''')
    parser.add_argument(
        "--log-level", dest='log_level', default='INFO',
        help="set log level")

    subparsers = parser.add_subparsers(help='sub-command help',
                                       dest='subparser_name')

    extract_features_subcmd_name = 'extract-features'
    extract_features_parser = subparsers.add_parser(
        extract_features_subcmd_name,
        help='extract features from given traces and store new results into '
        'a new db. traces already found in current db will be skipped.')
    extract_features_parser.add_argument(
        "--num-workers", dest='num_workers', type=int, default=None,
        help="number of parallel worker processes (default: half the number of CPU cores)")

    extract_features_parser.add_argument(
        "--worker-flush-interval", dest='worker_flush_interval',
        type=int, default=g_default_worker_flush_interval,
        help="make each worker flush new results to disk every time it has extracted "
        "features for this number of new traces. "
        "this is so that a worker crash doesn't cause to lose all new data. "
        "(default: {default})".format(default=g_default_worker_flush_interval))

    default_trace_features_db_fname = 'trace-features-db.dill'
    extract_features_parser.add_argument(
        "--db",
        metavar="db", default=default_trace_features_db_fname,
        help='path to trace features db. traces that already exist in db do not need to be processed again.'
        ' (default: {default})'.format(default=default_trace_features_db_fname))
    extract_features_parser.add_argument(
        "trace_file", nargs='+',
        help="path to a trace file")

    args = parser.parse_args()

    # assuming loglevel is bound to the string value obtained from the
    # command line argument. Convert to upper case to allow the user to
    # specify --log=DEBUG or --log=debug
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log_level)
    rootLogger = logging.getLogger()
    assert not rootLogger.hasHandlers()
    # using "my_handler" member is hacky but needed because there is
    # no "remove all handlers" api, so we have to remember our handler
    # in order to remove it later
    rootLogger.my_handler = logging.StreamHandler()
    rootLogger.my_formatter = logging.Formatter(
        fmt="%(processName)s(%(process)s): %(levelname) -10s %(asctime)s %(module)s:%(lineno)d, %(funcName)s(): %(message)s")
    rootLogger.my_handler.setFormatter(rootLogger.my_formatter)
    rootLogger.addHandler(rootLogger.my_handler)
    assert rootLogger.hasHandlers()
    rootLogger.setLevel(numeric_level)

    if args.subparser_name == extract_features_subcmd_name:
        _extract_features(args)
        pass

    pass
