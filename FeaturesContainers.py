
# this is mainly for abstracting the features about a trace, with a
# version, so that we can pickle this object for later use.
class FeaturesContainerV1(object):

    # "features" is meant to be an opaque object, but currently is a
    # vector of features, returned by FeatureExtractor::extract()
    def __init__(self, features):
        self.version = 1
        self.features = features
        pass

    pass
