#!/usr/bin/env python3

# go through the dill traces in a "traces" dir and show/assert some
# stats about the traces

import sys
import statistics
import os
import dill, pdb
import logging
from collections import defaultdict
from scipy import stats
import random
import argparse
import bisect
from sklearn.metrics import mean_squared_error

def get_total_sizes(pkts):
    down = up = 0
    for _, size in pkts:
        if size < 0:
            down += size
        else:
            up += size

    return (-1 * down), up


class Trace(object):
    def __init__(self, *, ID, label, total_bytes_up, total_bytes_down):
        self.ID = ID # should be uniq ID of this trace among all
                     # traces
        self.label = label
        self.total_bytes_up = total_bytes_up
        self.total_bytes_down = total_bytes_down
        pass

    pass


def do_it(expdir, classifier_spec='1'):

    classifiers = set(classifier_spec.split(','))

    # map from label to list of traces
    labels_dict = defaultdict(list)

    all_traces_list = []

    up_bytes_freq = defaultdict(int)
    down_bytes_freq = defaultdict(int)

    for dirpath, _, filenames in os.walk(expdir, topdown=False):
        # if not dirpath.endswith('traces'):
        #     continue
        for filename in filenames:
            if not filename.endswith('.dill'):
                continue
            fpath = os.path.join(dirpath, filename)
            with open(fpath, 'rb') as fp:
                pkts = dill.load(fp)
                down, up = get_total_sizes(pkts)

                label = dirpath

                trace_obj = Trace(ID=fpath, label=label,
                                total_bytes_up=up, total_bytes_down=down)

                labels_dict[label].append(trace_obj)

                all_traces_list.append(trace_obj)

                logging.debug('trace "{ID}": up= {up}, down= {down}'.format(
                    ID=fpath, up=up, down=down))

                up_bytes_freq[up] += 1
                down_bytes_freq[down] += 1

                pass
            pass
        pass

    #print(stats.entropy(list(down_bytes_freq.values())))

    # pick a random sample of each label's traces for training

    all_training_traces = []

    all_test_traces = []

    num_training_per_label = 60
    num_test_per_label = 30

    label_to_training_traces = {}
    label_to_test_traces = {}

    for label, traces in labels_dict.items():
        assert len(traces) >= (num_training_per_label + 20)

        tmplist = list(traces)
        random.shuffle(tmplist)

        label_to_training_traces[label] = tmplist[:num_training_per_label]

        label_to_test_traces[label] = tmplist[num_training_per_label:]

        assert (len(label_to_test_traces[label]) + len(label_to_training_traces[label])) == len(tmplist)

        all_training_traces.extend(label_to_training_traces[label])
        all_test_traces.extend(label_to_test_traces[label])
        pass


    # sort all training traces by total down bytes
    all_training_traces.sort(key=lambda trace: trace.total_bytes_down)

    # just double check
    prev = 0
    for trace in all_training_traces:
        assert trace.total_bytes_down >= prev
        prev = trace.total_bytes_down
        pass


    all_training_total_bytes_down = list(map(lambda trace: trace.total_bytes_down, all_training_traces))

    prev = 0
    for val in all_training_total_bytes_down:
        assert val >= prev
        prev = val
        pass

    class ClassifierResult(object):
        def __init__(self):
            self.num_correct = 0
            pass
        pass

    classifier_num_correct = defaultdict(int)

    num_tests = 0
    for trace in all_test_traces:
        num_tests += 1

        if '1' in classifiers:
            classifier_num_correct['1'] += 1 if classify1(all_training_traces, all_training_total_bytes_down, trace) else 0
            pass

        if '2' in classifiers:
            classifier_num_correct['2'] += 1 if classify2(label_to_training_traces, trace) else 0
            pass

        pass

    for classifier in sorted(classifiers):
        num_correct = classifier_num_correct[classifier]
        print('classifier {}'.format(classifier))
        print('num tests: {ntests}, num correct: {ncorrect}, accuracy: {acc:0.2f}'.format(
            ntests=num_tests, ncorrect=num_correct, acc=float(num_correct) / num_tests * 100))
        pass

    return


''' return true if the test trace is correctly classified; return false
otherwise
'''

def classify1(training_traces, training_traces_total_bytes_down,
              test_trace):

    # find out where this test trace would fit in the total bytes
    # array
    idx = bisect.bisect_left(training_traces_total_bytes_down,
                             test_trace.total_bytes_down)

    logging.debug('test trace: "{trace}" idx = {idx}'.format(
        trace=test_trace.ID, idx=idx))

    N = 40
    left = max(0, idx - int(N/2))
    right = min(len(training_traces), idx + int(N/2))

    label_counts = defaultdict(int)
    for i in range(left, right):
        training_label = training_traces[i].label
        logging.debug('training label: {}'.format(training_label))
        label_counts[training_label] += 1
        pass

    highest_count = max(label_counts.values())

    potential_labels = [k for k,v in label_counts.items() if v==highest_count]
    random.shuffle(potential_labels)
    picked_label = potential_labels[0]

    logging.debug('predict: {}, actual: {}'.format(picked_label, test_trace.label))

    # score_to_label = []
    # for label, traces in labels_dict.items():
    #     score_to_label.append((label, _score(

    return picked_label == test_trace.label

def classify2(label_to_training_traces,
              test_trace):

    logging.debug('testing trace {}'.format(test_trace.ID))

    label_to_mse = {}
    for label, training_traces in label_to_training_traces.items():
        training_total_bytes_down = [trace.total_bytes_down for trace in training_traces]
        test_total_bytes_down = [test_trace.total_bytes_down] * len(training_total_bytes_down)

        mse = mean_squared_error(training_total_bytes_down, test_total_bytes_down)
        label_to_mse[label] = mse

        logging.debug('  mse {} with training label {}'.format(mse, label))
        pass

    lowest_mse = min(label_to_mse.values())

    potential_labels = [k for k,v in label_to_mse.items() if v==lowest_mse]
    random.shuffle(potential_labels)
    picked_label = potential_labels[0]

    logging.debug('predict: {}, actual: {}'.format(picked_label, test_trace.label))

    return picked_label == test_trace.label

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=''
        )
    parser.add_argument(
        "--log-level", dest='log_level', default='WARNING',
        help="set log level")
    parser.add_argument(
        "-c", dest='classifier', default='1',
        help='comma separated list of classifiers, e.g., "1" or "1,2"')

    parser.add_argument("exp_dir",
                        metavar="exp_dir",
                        help='path to experiment output dir')

    args = parser.parse_args()

    # assuming loglevel is bound to the string value obtained from the
    # command line argument. Convert to upper case to allow the user to
    # specify --log=DEBUG or --log=debug
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log_level)
    rootLogger = logging.getLogger()
    assert not rootLogger.hasHandlers()
    # using "my_handler" member is hacky but needed because there is
    # no "remove all handlers" api, so we have to remember our handler
    # in order to remove it later
    rootLogger.my_handler = logging.StreamHandler()
    rootLogger.my_formatter = logging.Formatter(
        fmt="%(processName)s(%(process)s): %(levelname) -10s %(asctime)s %(module)s:%(lineno)d, %(funcName)s(): %(message)s",
        datefmt='%I:%M:%S')
    rootLogger.my_handler.setFormatter(rootLogger.my_formatter)
    rootLogger.addHandler(rootLogger.my_handler)
    assert rootLogger.hasHandlers()
    rootLogger.setLevel(numeric_level)

    expdir = args.exp_dir


    do_it(expdir, classifier_spec=args.classifier)

    pass


if __name__ == '__main__':
    main()
    pass

