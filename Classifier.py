import random
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from WebsiteCollection import Website
import logging
import statistics
import copy

class ClassifierParams(object):
    def __init__(self, num_websites, num_training_instances, num_test_instances):
        self.__num_websites = num_websites
        self.__num_training_instances = num_training_instances
        self.__num_test_instances = num_test_instances

    def get_num_websites(self):
        return self.__num_websites

    def get_num_training_instances(self):
        return self.__num_training_instances

    def get_num_test_instances(self):
        return self.__num_test_instances

    def __str__(self):
        return '''num sites: {numsites}
num training instances: {numtrain}
num test instances: {numtest}'''.format(numsites=self.__num_websites,
                                        numtrain=self.__num_training_instances,
                                        numtest=self.__num_test_instances)


class Classifier(object):
    def __init__(self, website_collection):
        self.__website_collection = website_collection
        pass

    # if optional "only_alt_test_[data,labels]" are lists, they will
    # be filled with instances/labels of only the alternate test
    # data/labels
    def __get_training_test_data(self, *, website_list, classifier_params,
                                 out_training_data, out_training_labels,
                                 out_test_data, out_test_labels,
                                 only_alt_test_data=None, only_alt_test_labels=None):
        num_websites = len(website_list)
        if classifier_params.get_num_websites() > 0:
            num_websites = min(num_websites, classifier_params.get_num_websites())
            pass
        num_websites_with_alt_test_data = 0

        for i in range(num_websites):
            # shallow copy of instance list, so our random.shuffles
            # operates on the copy
            instances = list(website_list[i].get_instances())
            label = website_list[i].get_label()
            random.shuffle(instances)

            num_training_instances = len(instances)
            if classifier_params.get_num_training_instances() > 0:
                num_training_instances = min(num_training_instances,
                                             classifier_params.get_num_training_instances())
                pass

            out_training_data.extend(instances[:num_training_instances])
            out_training_labels.extend([label] * num_training_instances)

            alt_test_instances = list(website_list[i].get_alt_test_instances())
            if not alt_test_instances:
                # using "instances" for both training and testing
                num_test_instances = len(instances) - num_training_instances
                if classifier_params.get_num_test_instances() > 0:
                    num_test_instances = min(num_test_instances, classifier_params.get_num_test_instances())
                    pass

                out_test_data.extend(instances[num_training_instances : num_training_instances + num_test_instances])

                if len(instances) < (num_training_instances + num_test_instances):
                    logging.warning('site "{label}" has only {total} instances'.format(
                        label=label, total=len(instances)))
                    pass
                pass
            else:
                # if there are alternate test traces, then use them
                num_test_instances = len(alt_test_instances)
                if classifier_params.get_num_test_instances() > 0:
                    num_test_instances = min(num_test_instances,
                                             classifier_params.get_num_test_instances())
                    pass

                sample = random.sample(alt_test_instances, num_test_instances)
                out_test_data.extend(sample)
                logging.debug('using {count} alt testing traces for label "{label}"'.format(
                    count=len(sample), label=label))

                if type(only_alt_test_data) is list:
                    only_alt_test_data.extend(sample)
                    pass
                if type(only_alt_test_labels) is list:
                    only_alt_test_labels.extend([label] * num_test_instances)
                    pass

                num_websites_with_alt_test_data += 1
                pass

            out_test_labels.extend([label] * num_test_instances)
            pass

        return num_websites, num_websites_with_alt_test_data

    def __vote(self, closest_distances_labels):
        labels = [label for _, label in closest_distances_labels]
        if len(set(labels)) == 1:
            return labels[0]
        return Website.UNMONITORED_LABEL

    def classifiy(self, knn, alexa_params = None, hs_params = None, unmonitored_params = None):
        training_data  = []
        training_labels = []
        test_data = []
        test_labels = []
        only_alt_test_data = []
        only_alt_test_labels = []

        total_num_sites = total_num_sites_with_alt_test_data = 0

        print("")
        if alexa_params:
            num_sites, num_sites_with_alt_test_data = \
                self.__get_training_test_data(website_list=self.__website_collection.get_alexa_websites(),
                                              classifier_params=alexa_params,
                                              out_training_data=training_data,
                                              out_training_labels=training_labels,
                                              out_test_data=test_data,
                                              out_test_labels=test_labels,
                                              only_alt_test_data=only_alt_test_data,
                                              only_alt_test_labels=only_alt_test_labels,
                                          )
            print('using {num_sites} alexa sites, with {num_train} training and {num_test} testing instances'.format(
                num_sites=num_sites, num_train=len(training_data), num_test=len(test_data)))
            total_num_sites += num_sites
            total_num_sites_with_alt_test_data += num_sites_with_alt_test_data
            pass

        if hs_params:
            assert False, 'not yet implemented'
            self.__get_training_test_data(website_list=self.__website_collection.get_hidden_services(),
                                          classifier_params=hs_params,
                                          out_training_data=training_data,
                                          out_training_labels=training_labels,
                                          out_test_data=test_data,
                                          out_test_labels=test_labels)

        if unmonitored_params:
            num_sites, _ = self.__get_training_test_data(website_list=self.__website_collection.get_unmonitored_websites(),
                                          classifier_params=unmonitored_params,
                                          out_training_data=training_data,
                                          out_training_labels=training_labels,
                                          out_test_data=test_data,
                                          out_test_labels=test_labels)
            print('using {num_sites} unmonitored sites'.format(num_sites=num_sites))
            total_num_sites += num_sites
            pass

        # print('Training Random Forest Classifier... with {num_train} training instances'.format(
        #     num_train=len(training_data)))

        accuracies = []
        accuracies_with_alt_test_data = []
        for i in range(1):
            model = RandomForestClassifier(n_jobs = -1, n_estimators=1000, oob_score = True)
            model.fit(training_data, training_labels)
            accuracy = 100.0 * model.score(test_data, test_labels)
            accuracies.append(accuracy)
            logging.debug("run i = {}:\nRF Classification Accuracy = {:.2f} %".format(i, accuracy))

            if len(only_alt_test_data) > 0:
                accuracy = 100.0 * model.score(only_alt_test_data, only_alt_test_labels)
                accuracies_with_alt_test_data.append(accuracy)
                logging.debug("RF Classification Accuracy of only labels with alternate test data = {:.2f} %".format(accuracy))
                pass
            pass
        print('mean RF accuracy from {num_runs} runs: {accuracy:.2f} %'.format(
            num_runs=len(accuracies), accuracy=statistics.mean(accuracies)),
              '(vs random choice 1/{N} = {accuracy:.2f} %)'.format(
                  N=total_num_sites, accuracy=100./total_num_sites))
        if len(only_alt_test_data) > 0:
            print('    of only labels with alternate test data: {accuracy:.2f} %'.format(
                accuracy=statistics.mean(accuracies_with_alt_test_data)),
                  '(vs random choice 1/{N} = {accuracy:.2f} %)'.format(
                      N=total_num_sites_with_alt_test_data, accuracy=100./total_num_sites_with_alt_test_data))
            pass

        #cross_validation_scores = cross_val_score(model, np.array(training_data), np.array(training_labels))
        #print("RF Cross Validation Score = ", cross_validation_scores.mean())

        #Original Implementaion does this only in case of open world classification
        if unmonitored_params:
            print('Generating Leaves...')
            training_leaves = model.apply(training_data)
            test_leaves = model.apply(test_data)

            training_leaves = [np.array(training_leaf, dtype=int) for training_leaf in training_leaves]
            test_leaves = [np.array(test_leaf, dtype=int) for test_leaf in test_leaves]

            true_positive = 0
            false_positive = 0

            print('Calculating Distances...')
            for test_leaf_idx in range(len(test_leaves)):
                test_leaf = test_leaves[test_leaf_idx]
                true_label = test_labels[test_leaf_idx]
                dist_predicted_labels = [] #List of (distance, predicated_label) pairs

                for training_leaf_idx in range(len(training_leaves)):
                    training_leaf = training_leaves[training_leaf_idx]
                    predictaed_label = training_labels[training_leaf_idx]

                    distance = np.sum(training_leaf != test_leaf) / float(training_leaf.size)
                    if distance == 1.0:
                        continue
                    dist_predicted_labels.append((distance, predictaed_label))

                closest_distances_labels = sorted(dist_predicted_labels)[:knn]
                classified_label =  self.__vote(closest_distances_labels)

                if true_label != Website.UNMONITORED_LABEL and true_label == classified_label:
                    true_positive += 1

                if true_label == Website.UNMONITORED_LABEL and true_label != classified_label:
                    false_positive += 1

            num_unmonitored_test_instances = test_labels.count(Website.UNMONITORED_LABEL)
            num_monitored_test_instances = len(test_labels) - num_unmonitored_test_instances

            true_positive_rate = true_positive / float(num_monitored_test_instances)
            false_positive_rate = false_positive / float(num_unmonitored_test_instances)

            print("True Positive Count = %d / %d" %(true_positive, num_monitored_test_instances))
            print("False Positive Count = %d / %d" %(false_positive, num_unmonitored_test_instances))
            print("True Positive Rate: ", true_positive_rate)
            print("False Positive Rate: ", false_positive_rate)

